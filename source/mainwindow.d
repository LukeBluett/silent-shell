import gio.Application: GioApplication = Application;
import gtk.Application, gtk.ApplicationWindow, gtk.Builder, gtk.Button, gtk.Entry, gtk.TextView;
import std.stdio, core.stdc.stdlib, std.process;

/**
 *	Main Window
 */

class MainWindow {
private {
	Builder builder;
	Window window;
	Button btnExecute;
	Entry entryCommand;
	TextView viewOutput;
}

public {
	this(string gladefile) {
		this.builder = new Builder();
		this.builder.addFromFile(gladefile);
		this.window = cast(ApplicationWindow)builder.getObject("window");
		this.btnExecute = cast(Button)builder.getObject("btnExecute");
		this.btnExecute.addOnButtonClicked(&on_btnExecute_clicked);
		this.entryCommand = cast(Entry)builder.getObject("entryCommand");
		this.viewOutput = cast(TextView)builder.getObject("viewOutput");
	}

	bool on_btnExecute_clicked(Event e, Widget w) {
		auto command = this.entryCommand.getText();
		auto result = executeShell(command);
		this.viewOutput.appendText(
			"*****\nStart of Command: " ~ 
			command ~
			"\n*****\n\n"
		);
		this.viewOutput.appendText(result.output);
		this.viewOutput.appendText(
			"\n*****\nEnd of Command: " ~
			command ~
			"\n*****\n\n"
		);
		return true;
	}

	void show() {
		this.window.showAll();
	}

	void hide() {
		this.window.hide();
	}
}
}
