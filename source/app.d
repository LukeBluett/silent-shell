
import gio.Application: GioApplication = Application;

import gtk.Application;
import gtk.ApplicationWindow;
import gtk.Builder;
import gtk.Button;
import gtk.Entry;
import gtk.TextView;

import std.stdio;
import core.stdc.stdlib;
import std.process;

/**
 * Usage ./gladeText /path/to/your/glade/file.glade
 *
 */

int main(string[] args) {
	string gladefile = "sample.glade";

	auto application = new Application(
		"org.gtkd.demo.builder.builderTest", 
		GApplicationFlags.FLAGS_NONE
	);

	void buildAndDisplay(GioApplication a) {
		auto builder = new Builder();
		if( ! builder.addFromFile(gladefile) ) {
			writeln("Oops, could not create Glade object, check your glade file ;)");
			exit(1);
		}
		auto window = cast(ApplicationWindow)builder.getObject("window");
		window.setApplication(application);
		if (window !is null) {
			window.setTitle("This is a glade application window");
			auto button = cast(Button)builder.getObject("btnExecute");
			auto entry = cast(Entry)builder.getObject("entryCommand");
			auto view = cast(TextView)builder.getObject("viewOutput");
			if(button !is null) {
				button.addOnClicked( 
					delegate void(Button aux) { 
						auto ls = executeShell(entry.getText());
						view.appendText(
							"*****\n Start, Command: " ~ 
							entry.getText() ~ 
							"\n*****\n\n"
						);
						view.appendText(ls.output);
						view.appendText(
							"\n*****\n End, Command: " ~ 
							entry.getText() ~ 
							"\n*****\n\n"
						);
						entry.setText("");
					} 
				);
				window.showAll();
			} else {
				writeln("No button in the window?");
				exit(1);
			}
		} else {
			writeln("No window?");
			exit(1);
		}
	}

	application.addOnActivate(&buildAndDisplay);
	return application.run(args);
}
